package br.com.uol.desafioandroid.domain.enums;

import android.content.Context;

import br.com.uol.desafioandroid.R;

/**
 * Created by ricarlosilva on 30/11/2017.
 */

public enum ListEnum {
    SHOTS(R.string.list_shots),
    DEBUTS(R.string.list_debuts),
    TEAMS(R.string.list_teams),
    REBOUNDS(R.string.list_rebounds),
    PLAYOFFS(R.string.list_playoffs),
    ANIMATED(R.string.list_animated),
    ATTACHMENTS(R.string.list_attachments);

    private final int stringRes;

    public String getLabel(Context context){
        return context.getString(stringRes);
    }

    ListEnum(int stringRes) {
        this.stringRes = stringRes;
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
