package br.com.uol.desafioandroid.domain.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserModel implements Serializable {

    @SerializedName("comments_received_count")
    private int commentsReceivedCount;

    @SerializedName("buckets_url")
    private String bucketsUrl;

    @SerializedName("following_url")
    private String followingUrl;

    @SerializedName("bio")
    private String bio;

    @SerializedName("projects_count")
    private int projectsCount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("type")
    private String type;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("shots_url")
    private String shotsUrl;

    @SerializedName("links")
    private LinkModel links;

    @SerializedName("id")
    private int id;

    @SerializedName("teams_count")
    private int teamsCount;

    @SerializedName("can_upload_shot")
    private boolean canUploadShot;

    @SerializedName("likes_url")
    private String likesUrl;

    @SerializedName("likes_received_count")
    private int likesReceivedCount;

    @SerializedName("pro")
    private boolean pro;

    @SerializedName("followers_url")
    private String followersUrl;

    @SerializedName("buckets_count")
    private int bucketsCount;

    @SerializedName("followings_count")
    private int followingsCount;

    @SerializedName("rebounds_received_count")
    private int reboundsReceivedCount;

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("teams_url")
    private String teamsUrl;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("followers_count")
    private int followersCount;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private String location;

    @SerializedName("projects_url")
    private String projectsUrl;

    @SerializedName("shots_count")
    private int shotsCount;

    @SerializedName("username")
    private String username;

    public int getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    public String getBucketsUrl() {
        return bucketsUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public String getBio() {
        return bio;
    }

    public int getProjectsCount() {
        return projectsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getType() {
        return type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getShotsUrl() {
        return shotsUrl;
    }

    public LinkModel getLinks() {
        return links;
    }

    public int getId() {
        return id;
    }

    public int getTeamsCount() {
        return teamsCount;
    }

    public boolean isCanUploadShot() {
        return canUploadShot;
    }

    public String getLikesUrl() {
        return likesUrl;
    }

    public int getLikesReceivedCount() {
        return likesReceivedCount;
    }

    public boolean isPro() {
        return pro;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public int getBucketsCount() {
        return bucketsCount;
    }

    public int getFollowingsCount() {
        return followingsCount;
    }

    public int getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public String getTeamsUrl() {
        return teamsUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getProjectsUrl() {
        return projectsUrl;
    }

    public int getShotsCount() {
        return shotsCount;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return
                "UserModel{" +
                        "comments_received_count = '" + commentsReceivedCount + '\'' +
                        ",buckets_url = '" + bucketsUrl + '\'' +
                        ",following_url = '" + followingUrl + '\'' +
                        ",bio = '" + bio + '\'' +
                        ",projects_count = '" + projectsCount + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",type = '" + type + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",shots_url = '" + shotsUrl + '\'' +
                        ",links = '" + links + '\'' +
                        ",id = '" + id + '\'' +
                        ",teams_count = '" + teamsCount + '\'' +
                        ",can_upload_shot = '" + canUploadShot + '\'' +
                        ",likes_url = '" + likesUrl + '\'' +
                        ",likes_received_count = '" + likesReceivedCount + '\'' +
                        ",pro = '" + pro + '\'' +
                        ",followers_url = '" + followersUrl + '\'' +
                        ",buckets_count = '" + bucketsCount + '\'' +
                        ",followings_count = '" + followingsCount + '\'' +
                        ",rebounds_received_count = '" + reboundsReceivedCount + '\'' +
                        ",likes_count = '" + likesCount + '\'' +
                        ",teams_url = '" + teamsUrl + '\'' +
                        ",avatar_url = '" + avatarUrl + '\'' +
                        ",html_url = '" + htmlUrl + '\'' +
                        ",followers_count = '" + followersCount + '\'' +
                        ",name = '" + name + '\'' +
                        ",location = '" + location + '\'' +
                        ",projects_url = '" + projectsUrl + '\'' +
                        ",shots_count = '" + shotsCount + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }
}