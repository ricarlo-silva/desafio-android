package br.com.uol.desafioandroid.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import br.com.uol.desafioandroid.App;
import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.domain.model.ShotsModel;
import br.com.uol.desafioandroid.utils.AndroidUtils;

/**
 * Created by ricarlosilva on 27/11/2017.
 */

public class ShotsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static Context context;
    private static int numColumns;
    private List<ShotsModel> items;
    private OnItemClickListener onItemClickListener;


    public interface OnItemClickListener {
        void onItemClicked(ShotsModel item, ImageView ivPhoto);

    }

    public void setNumColumns(int numColumns){
        this.numColumns = numColumns;
    }

    public ShotsAdapter(Context context, OnItemClickListener onItemClickListener, int numColumns, List<ShotsModel> items) {
        this.onItemClickListener = onItemClickListener;
        ShotsAdapter.context = context;
        ShotsAdapter.numColumns = numColumns;
        this.items = items;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivPhoto;
        private TextView tvViews;
        private TextView tvComments;
        private TextView tvLikes;
        private ImageView ivProfile;
        private TextView tvProfile;

        private ViewHolder(View itemView) {
            super(itemView);

            ivPhoto = itemView.findViewById(R.id.ivPhoto);
            ivPhoto.setLayoutParams(AndroidUtils.calcHeight(context, numColumns, 26, ivPhoto.getLayoutParams(), new Pair<>(4, 3)));

            tvViews = itemView.findViewById(R.id.tvViews);
            tvComments = itemView.findViewById(R.id.tvComments);
            tvLikes = itemView.findViewById(R.id.tvLikes);
            ivProfile = itemView.findViewById(R.id.ivProfile);
            tvProfile = itemView.findViewById(R.id.tvProfile);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_shots, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ShotsModel item = items.get(position);
        final ViewHolder viewHolder = (ViewHolder) holder;

        viewHolder.ivPhoto.setLayoutParams(AndroidUtils.calcHeight(context, numColumns, 26, viewHolder.ivPhoto.getLayoutParams(), new Pair<>(4, 3)));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onItemClicked(item, viewHolder.ivPhoto);
            }
        });

        if (item.getImages() != null) {
            Glide.with(context).load(item.getImages().getNormal()).into(viewHolder.ivPhoto);
        }

        viewHolder.tvViews.setText(String.valueOf(item.getViewsCount()));
        viewHolder.tvComments.setText(String.valueOf(item.getCommentsCount()));
        viewHolder.tvLikes.setText(String.valueOf(item.getLikesCount()));

        if (item.getUser() != null) {
            viewHolder.tvProfile.setText(item.getUser().getName());
            Glide.with(context).load(item.getUser().getAvatarUrl()).into(viewHolder.ivProfile);
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void add(ShotsModel item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }

    public void addAll(List<ShotsModel> items) {
        for (int i = 0; i < items.size(); i++) {
            add(items.get(i));
        }
    }

    private void remove(ShotsModel item) {
        int position = items.indexOf(item);
        if (position > -1) {
            items.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    private ShotsModel getItem(int position) {
        return items.get(position);
    }

}
