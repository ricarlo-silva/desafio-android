package br.com.uol.desafioandroid.domain.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LinkModel implements Serializable {

    @SerializedName("twitter")
    private String twitter;

    @SerializedName("web")
    private String web;

    public String getTwitter() {
        return twitter;
    }

    public String getWeb() {
        return web;
    }

    @Override
    public String toString() {
        return
                "LinkModel{" +
                        "twitter = '" + twitter + '\'' +
                        ",web = '" + web + '\'' +
                        "}";
    }
}