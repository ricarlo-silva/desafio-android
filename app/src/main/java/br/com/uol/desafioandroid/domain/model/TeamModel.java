package br.com.uol.desafioandroid.domain.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TeamModel implements Serializable {

    @SerializedName("comments_received_count")
    private int commentsReceivedCount;

    @SerializedName("buckets_url")
    private String bucketsUrl;

    @SerializedName("members_url")
    private String membersUrl;

    @SerializedName("following_url")
    private String followingUrl;

    @SerializedName("bio")
    private String bio;

    @SerializedName("projects_count")
    private int projectsCount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("type")
    private String type;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("shots_url")
    private String shotsUrl;

    @SerializedName("links")
    private LinkModel links;

    @SerializedName("id")
    private int id;

    @SerializedName("can_upload_shot")
    private boolean canUploadShot;

    @SerializedName("likes_url")
    private String likesUrl;

    @SerializedName("likes_received_count")
    private int likesReceivedCount;

    @SerializedName("team_shots_url")
    private String teamShotsUrl;

    @SerializedName("pro")
    private boolean pro;

    @SerializedName("followers_url")
    private String followersUrl;

    @SerializedName("buckets_count")
    private int bucketsCount;

    @SerializedName("followings_count")
    private int followingsCount;

    @SerializedName("rebounds_received_count")
    private int reboundsReceivedCount;

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("followers_count")
    private int followersCount;

    @SerializedName("name")
    private String name;

    @SerializedName("location")
    private String location;

    @SerializedName("members_count")
    private int membersCount;

    @SerializedName("projects_url")
    private String projectsUrl;

    @SerializedName("shots_count")
    private int shotsCount;

    @SerializedName("username")
    private String username;

    public int getCommentsReceivedCount() {
        return commentsReceivedCount;
    }

    public String getBucketsUrl() {
        return bucketsUrl;
    }

    public String getMembersUrl() {
        return membersUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public String getBio() {
        return bio;
    }

    public int getProjectsCount() {
        return projectsCount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getType() {
        return type;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getShotsUrl() {
        return shotsUrl;
    }

    public LinkModel getLinks() {
        return links;
    }

    public int getId() {
        return id;
    }

    public boolean isCanUploadShot() {
        return canUploadShot;
    }

    public String getLikesUrl() {
        return likesUrl;
    }

    public int getLikesReceivedCount() {
        return likesReceivedCount;
    }

    public String getTeamShotsUrl() {
        return teamShotsUrl;
    }

    public boolean isPro() {
        return pro;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public int getBucketsCount() {
        return bucketsCount;
    }

    public int getFollowingsCount() {
        return followingsCount;
    }

    public int getReboundsReceivedCount() {
        return reboundsReceivedCount;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public int getMembersCount() {
        return membersCount;
    }

    public String getProjectsUrl() {
        return projectsUrl;
    }

    public int getShotsCount() {
        return shotsCount;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return
                "TeamModel{" +
                        "comments_received_count = '" + commentsReceivedCount + '\'' +
                        ",buckets_url = '" + bucketsUrl + '\'' +
                        ",members_url = '" + membersUrl + '\'' +
                        ",following_url = '" + followingUrl + '\'' +
                        ",bio = '" + bio + '\'' +
                        ",projects_count = '" + projectsCount + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",type = '" + type + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",shots_url = '" + shotsUrl + '\'' +
                        ",links = '" + links + '\'' +
                        ",id = '" + id + '\'' +
                        ",can_upload_shot = '" + canUploadShot + '\'' +
                        ",likes_url = '" + likesUrl + '\'' +
                        ",likes_received_count = '" + likesReceivedCount + '\'' +
                        ",team_shots_url = '" + teamShotsUrl + '\'' +
                        ",pro = '" + pro + '\'' +
                        ",followers_url = '" + followersUrl + '\'' +
                        ",buckets_count = '" + bucketsCount + '\'' +
                        ",followings_count = '" + followingsCount + '\'' +
                        ",rebounds_received_count = '" + reboundsReceivedCount + '\'' +
                        ",likes_count = '" + likesCount + '\'' +
                        ",avatar_url = '" + avatarUrl + '\'' +
                        ",html_url = '" + htmlUrl + '\'' +
                        ",followers_count = '" + followersCount + '\'' +
                        ",name = '" + name + '\'' +
                        ",location = '" + location + '\'' +
                        ",members_count = '" + membersCount + '\'' +
                        ",projects_url = '" + projectsUrl + '\'' +
                        ",shots_count = '" + shotsCount + '\'' +
                        ",username = '" + username + '\'' +
                        "}";
    }
}