package br.com.uol.desafioandroid.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.domain.enums.ListEnum;
import br.com.uol.desafioandroid.domain.enums.SortEnum;
import br.com.uol.desafioandroid.domain.model.ShotsModel;
import br.com.uol.desafioandroid.domain.repository.OnResponseListener;
import br.com.uol.desafioandroid.domain.repository.shots.ShotsRepository;
import br.com.uol.desafioandroid.ui.adapter.GridEndlessRecyclerViewScrollListener;
import br.com.uol.desafioandroid.ui.adapter.ShotsAdapter;

public class MainActivity extends BaseActivity implements ShotsAdapter.OnItemClickListener {

    private ShotsAdapter adapter;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView rvShots;
    private Spinner spinnerSort;
    private Spinner spinnerList;
    private ProgressBar progressBar;
    private ImageView tvGrid;
    private ImageView ivOptionCard;
    private ImageView ivOptionGrid;
    private LinearLayout containerGrid;
    private Toolbar toolbar;
    private ShotsRepository shotsRepository = new ShotsRepository();
    private int numColumns = 1;
    private GridEndlessRecyclerViewScrollListener scrollListener;
    private GridLayoutManager layoutManager;

    private String sort = null;
    private String list = null;
    private HashMap<String, String> mapSort = new HashMap<>();
    private HashMap<String, String> mapList = new HashMap<>();

    @Override
    public void onItemClicked(ShotsModel item, ImageView ivPhoto) {

        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_SHOTS, item);
        ActivityOptionsCompat options = ActivityOptionsCompat.
                makeSceneTransitionAnimation(this, ivPhoto, ViewCompat.getTransitionName(ivPhoto));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ivOptionCard = findViewById(R.id.ivOptionCard);
        ivOptionCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numColumns = 1;
                resetLayout();
            }
        });


        ivOptionGrid = findViewById(R.id.ivOptionGrid);
        ivOptionGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                numColumns = 2;
                resetLayout();
            }
        });


        tvGrid = findViewById(R.id.tvGrid);
        containerGrid = findViewById(R.id.containerGrid);
        tvGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                containerGrid.setVisibility(containerGrid.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        toolbar = findViewById(R.id.toolbar);
        setupToolBar(toolbar, "", false);

        progressBar = findViewById(R.id.progressBar);

        initSortAndList();

        swipeRefresh = findViewById(R.id.swipeRefresh);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                resetAll();
            }
        });

        adapter = new ShotsAdapter(this, this, numColumns, new ArrayList<ShotsModel>());
        rvShots = findViewById(R.id.rvShots);
        layoutManager = new GridLayoutManager(this, numColumns);
        rvShots.setLayoutManager(layoutManager);
        rvShots.setAdapter(adapter);

        scrollListener = new GridEndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems(int page) {
                loadData(page);
            }
        };

        rvShots.addOnScrollListener(scrollListener);
        this.loadData(scrollListener.start());
    }


    private void initSortAndList() {

        mapSort.put(SortEnum.POPULAR.getLabel(this), SortEnum.POPULAR.toString());
        mapSort.put(SortEnum.RECENT.getLabel(this), SortEnum.RECENT.toString());
        mapSort.put(SortEnum.VIEWS.getLabel(this), SortEnum.VIEWS.toString());
        mapSort.put(SortEnum.COMMENTS.getLabel(this), SortEnum.COMMENTS.toString());


        ArrayAdapter adapterSort = ArrayAdapter.createFromResource(this, R.array.sort, R.layout.simple_spinner_item);
        adapterSort.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerSort = findViewById(R.id.spinnerSort);
        spinnerSort.setAdapter(adapterSort);

        spinnerSort.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = adapterView.getAdapter().getItem(i).toString();

                String option = mapSort.get(selected);
                if (!option.equals(sort)) {
                    sort = option.equals(SortEnum.POPULAR.toString()) ? null : option;
                    resetAll();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mapList.put(ListEnum.SHOTS.getLabel(this), ListEnum.SHOTS.toString());
        mapList.put(ListEnum.DEBUTS.getLabel(this), ListEnum.DEBUTS.toString());
        mapList.put(ListEnum.TEAMS.getLabel(this), ListEnum.TEAMS.toString());
        mapList.put(ListEnum.REBOUNDS.getLabel(this), ListEnum.REBOUNDS.toString());
        mapList.put(ListEnum.PLAYOFFS.getLabel(this), ListEnum.PLAYOFFS.toString());
        mapList.put(ListEnum.ANIMATED.getLabel(this), ListEnum.ANIMATED.toString());
        mapList.put(ListEnum.ATTACHMENTS.getLabel(this), ListEnum.ATTACHMENTS.toString());


        ArrayAdapter adapterList = ArrayAdapter.createFromResource(this, R.array.list, R.layout.simple_spinner_item);
        adapterList.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinnerList = findViewById(R.id.spinnerList);
        spinnerList.setAdapter(adapterList);

        spinnerList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selected = adapterView.getAdapter().getItem(i).toString();

                String option = mapList.get(selected);
                if (!option.equals(list)) {
                    list = option.equals(ListEnum.SHOTS.toString()) ? null : option;
                    resetAll();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void resetLayout(){
        containerGrid.setVisibility(View.GONE);
        layoutManager.setSpanCount(numColumns);
        adapter.setNumColumns(numColumns);
        adapter.notifyDataSetChanged();
    }

    private void resetAll() {

        adapter.clear();
        loadData(scrollListener.start());

    }


    private void loadData(int page) {
        progressBar.setVisibility(View.VISIBLE);

        shotsRepository.getShots(page, sort, list, new OnResponseListener<List<ShotsModel>>() {
            @Override
            public void onSuccess(List<ShotsModel> data) {
                swipeRefresh.setRefreshing(false);
                progressBar.setVisibility(View.GONE);

                adapter.addAll(data);
                if (data.isEmpty()) {
                    scrollListener.isLastPage = true;
                    showMessage(getCurrentFocus(), getString(R.string.main_activity_msg_end_shots));
                }
            }

            @Override
            public void onError(String message) {
                swipeRefresh.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                showMessage(getCurrentFocus(), message);
            }
        });
    }

}
