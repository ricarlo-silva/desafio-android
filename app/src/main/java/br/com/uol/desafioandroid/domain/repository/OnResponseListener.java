package br.com.uol.desafioandroid.domain.repository;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public interface OnResponseListener<T> {
    void onSuccess(T data);

    void onError(String message);
}
