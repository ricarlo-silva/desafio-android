package br.com.uol.desafioandroid;

import android.app.Application;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        return instance;
    }
}
