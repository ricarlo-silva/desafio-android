package br.com.uol.desafioandroid.domain.repository.model;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public class ApiParseError {

    public static ErrorResponse parse(String body){

        Type type = new TypeToken<ErrorResponse>() {}.getType();
        return new Gson().fromJson(body, type);
    }
}
