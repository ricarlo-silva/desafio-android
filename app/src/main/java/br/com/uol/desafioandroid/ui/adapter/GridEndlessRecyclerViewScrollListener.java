package br.com.uol.desafioandroid.ui.adapter;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by ricarlosilva on 30/11/2017.
 */

public abstract class GridEndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {

    private GridLayoutManager gridLayoutManager;
    private int previousItemCount;
    private boolean loading;
    public boolean isLastPage;
    private final int startingPageIndex = 0;
    public int currentPage = startingPageIndex;

    public GridEndlessRecyclerViewScrollListener(GridLayoutManager gridLayoutManager) {
        this.gridLayoutManager = gridLayoutManager;
        this.reset();
    }


    @Override
    public void onScrolled(RecyclerView view, int dx, int dy) {
        if (dy > 0) {
            int itemCount = gridLayoutManager.getItemCount();

            if (itemCount != previousItemCount) {
                loading = false;
            }

            if (!loading && !isLastPage && gridLayoutManager.findLastVisibleItemPosition() >= itemCount - 1) {
                previousItemCount = itemCount;
                loading = true;
                currentPage++;
                loadMoreItems(currentPage);
            }
        }
    }

    public int start() {
        this.reset();
        this.currentPage++;
        return currentPage;
    }

    public void reset() {
        this.loading = false;
        this.isLastPage = false;
        this.currentPage = startingPageIndex;
        this.previousItemCount = -1;
    }

    protected abstract void loadMoreItems(int page);
}
