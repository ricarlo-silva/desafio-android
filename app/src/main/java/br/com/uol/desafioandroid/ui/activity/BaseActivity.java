package br.com.uol.desafioandroid.ui.activity;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public class BaseActivity extends AppCompatActivity {

    protected void showMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    protected void setupToolBar(Toolbar toolbar, String title, boolean upNavigation) {
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(upNavigation);

            if (title != null) {
                getSupportActionBar().setTitle(title);
            }

        }

    }

}
