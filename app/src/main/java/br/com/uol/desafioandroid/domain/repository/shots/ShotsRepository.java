package br.com.uol.desafioandroid.domain.repository.shots;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.List;

import br.com.uol.desafioandroid.App;
import br.com.uol.desafioandroid.Constants;
import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.domain.model.ShotsModel;
import br.com.uol.desafioandroid.domain.repository.OnResponseListener;
import br.com.uol.desafioandroid.domain.repository.ServiceGenerator;
import br.com.uol.desafioandroid.domain.repository.model.ApiParseError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public class ShotsRepository {


    private ShotsAPI shotsAPI = ServiceGenerator.createService(ShotsAPI.class, Constants.BASE_URL);

    public void getShots(int page, @Nullable String sort, @Nullable String list, final OnResponseListener<List<ShotsModel>> callback) {

        shotsAPI.getShots(page, sort, list, Constants.TOKEN).enqueue(new Callback<List<ShotsModel>>() {
            @Override
            public void onResponse(@NonNull Call<List<ShotsModel>> call, @NonNull Response<List<ShotsModel>> response) {

                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    if (response.body() != null) {
                        callback.onError(ApiParseError.parse(response.body().toString()).getMessage());

                    } else {
                        callback.onError(App.getInstance().getString(R.string.api_generic_error));

                    }

                }

            }

            @Override
            public void onFailure(@NonNull Call<List<ShotsModel>> call, @NonNull Throwable t) {
                callback.onError(t.getMessage());
            }

        });

    }

}
