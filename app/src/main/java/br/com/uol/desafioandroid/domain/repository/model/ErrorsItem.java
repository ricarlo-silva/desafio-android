package br.com.uol.desafioandroid.domain.repository.model;

import com.google.gson.annotations.SerializedName;


public class ErrorsItem {

    @SerializedName("attribute")
    private String attribute;

    @SerializedName("message")
    private String message;

    public String getAttribute() {
        return attribute;
    }

    public String getMessage() {
        return message;
    }

}