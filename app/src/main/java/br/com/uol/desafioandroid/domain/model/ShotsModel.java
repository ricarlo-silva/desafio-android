package br.com.uol.desafioandroid.domain.model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ShotsModel implements Serializable {

    @SerializedName("buckets_url")
    private String bucketsUrl;

    @SerializedName("rebounds_url")
    private String reboundsUrl;

    @SerializedName("rebounds_count")
    private int reboundsCount;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("title")
    private String title;

    @SerializedName("attachments_url")
    private String attachmentsUrl;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("comments_url")
    private String commentsUrl;

    @SerializedName("id")
    private int id;

    @SerializedName("views_count")
    private int viewsCount;

    @SerializedName("height")
    private int height;

    @SerializedName("images")
    private ImageModel images;

    @SerializedName("likes_url")
    private String likesUrl;

    @SerializedName("team")
    private TeamModel team;

    @SerializedName("buckets_count")
    private int bucketsCount;

    @SerializedName("tags")
    private List<String> tags;

    @SerializedName("likes_count")
    private int likesCount;

    @SerializedName("comments_count")
    private int commentsCount;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("width")
    private int width;

    @SerializedName("animated")
    private boolean animated;

    @SerializedName("attachments_count")
    private int attachmentsCount;

    @SerializedName("projects_url")
    private String projectsUrl;

    @SerializedName("user")
    private UserModel user;

    public String getBucketsUrl() {
        return bucketsUrl;
    }

    public String getReboundsUrl() {
        return reboundsUrl;
    }

    public int getReboundsCount() {
        return reboundsCount;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getTitle() {
        return title;
    }

    public String getAttachmentsUrl() {
        return attachmentsUrl;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public int getId() {
        return id;
    }

    public int getViewsCount() {
        return viewsCount;
    }

    public int getHeight() {
        return height;
    }

    public ImageModel getImages() {
        return images;
    }

    public String getLikesUrl() {
        return likesUrl;
    }

    public Object getTeam() {
        return team;
    }

    public int getBucketsCount() {
        return bucketsCount;
    }

    public List<String> getTags() {
        return tags;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public int getCommentsCount() {
        return commentsCount;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public int getWidth() {
        return width;
    }

    public boolean isAnimated() {
        return animated;
    }

    public int getAttachmentsCount() {
        return attachmentsCount;
    }

    public String getProjectsUrl() {
        return projectsUrl;
    }

    public UserModel getUser() {
        return user;
    }

    @Override
    public String toString() {
        return
                "ShotsModel{" +
                        "buckets_url = '" + bucketsUrl + '\'' +
                        ",rebounds_url = '" + reboundsUrl + '\'' +
                        ",rebounds_count = '" + reboundsCount + '\'' +
                        ",description = '" + description + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",title = '" + title + '\'' +
                        ",attachments_url = '" + attachmentsUrl + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",comments_url = '" + commentsUrl + '\'' +
                        ",id = '" + id + '\'' +
                        ",views_count = '" + viewsCount + '\'' +
                        ",height = '" + height + '\'' +
                        ",images = '" + images + '\'' +
                        ",likes_url = '" + likesUrl + '\'' +
                        ",team = '" + team + '\'' +
                        ",buckets_count = '" + bucketsCount + '\'' +
                        ",tags = '" + tags + '\'' +
                        ",likes_count = '" + likesCount + '\'' +
                        ",comments_count = '" + commentsCount + '\'' +
                        ",html_url = '" + htmlUrl + '\'' +
                        ",width = '" + width + '\'' +
                        ",animated = '" + animated + '\'' +
                        ",attachments_count = '" + attachmentsCount + '\'' +
                        ",projects_url = '" + projectsUrl + '\'' +
                        ",user = '" + user + '\'' +
                        "}";
    }
}