package br.com.uol.desafioandroid.ui.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import br.com.uol.desafioandroid.R;
import br.com.uol.desafioandroid.domain.model.ShotsModel;

public class DetailsActivity extends BaseActivity {

    public static final String EXTRA_SHOTS = "shots";
    private ImageView ivProfile;
    private TextView tvProfile;
    private TextView tvLikes;
    private TextView tvViews;
    private TextView tvDescription;
    private ImageView ivPhoto;
    private TextView tvShare;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        final ShotsModel shotsModel = (ShotsModel) getIntent().getSerializableExtra(EXTRA_SHOTS);

        ivProfile = findViewById(R.id.ivProfile);
        tvProfile = findViewById(R.id.tvProfile);
        tvShare = findViewById(R.id.tvShare);
        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareShot(shotsModel);
            }
        });

        tvLikes = findViewById(R.id.tvLikes);
        tvViews = findViewById(R.id.tvViews);
        tvDescription = findViewById(R.id.tvDescription);

        tvViews.setText(String.valueOf(shotsModel.getViewsCount()));
        tvLikes.setText(String.valueOf(shotsModel.getLikesCount()));

        if (shotsModel.getDescription() != null) {
            tvDescription.setText(Html.fromHtml(shotsModel.getDescription()));
        }

        ivPhoto = findViewById(R.id.ivPhoto);
        toolbar = findViewById(R.id.toolbar);
        setupToolBar(toolbar, shotsModel.getTitle(), true);

        supportPostponeEnterTransition();

        if (shotsModel.getImages() != null) {
            loadImage(shotsModel.getImages().getNormal(), ivPhoto);
        }


        if (shotsModel.getUser() != null) {
            loadImage(shotsModel.getUser().getAvatarUrl(), ivProfile);
            tvProfile.setText(shotsModel.getUser().getName());
        }

    }

    private void loadImage(String path, ImageView imageView) {
        Glide.with(this)
                .load(path)
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        supportStartPostponedEnterTransition();
                        return false;
                    }
                })
                .apply(new RequestOptions()
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(imageView);
    }

    private void shareShot(ShotsModel shotsModel) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shotsModel.getHtmlUrl());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
