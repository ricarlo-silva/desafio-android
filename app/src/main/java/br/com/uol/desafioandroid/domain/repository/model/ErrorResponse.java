package br.com.uol.desafioandroid.domain.repository.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ErrorResponse {

    @SerializedName("message")
    private String message;

    @SerializedName("errors")
    private List<ErrorsItem> errors;

    public String getMessage() {
        return message;
    }

    public List<ErrorsItem> getErrors() {
        return errors;
    }

}