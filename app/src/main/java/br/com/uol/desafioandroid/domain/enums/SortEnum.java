package br.com.uol.desafioandroid.domain.enums;

import android.content.Context;

import br.com.uol.desafioandroid.R;

/**
 * Created by ricarlosilva on 30/11/2017.
 */

public enum SortEnum { //popular //recent // views // comments
    POPULAR(R.string.sort_popular),
    RECENT(R.string.sort_resent),
    VIEWS(R.string.sort_most_viewed),
    COMMENTS(R.string.sort_most_commented);

    private final int stringRes;

    public String getLabel(Context context){
        return context.getString(stringRes);
    }

    SortEnum(int stringRes) {
        this.stringRes = stringRes;
    }

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
