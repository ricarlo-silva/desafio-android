package br.com.uol.desafioandroid.utils;

import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by ricarlo on 29/11/2017.
 */

public class AndroidUtils {

    public static boolean isNetworkAvaliable(@NonNull Context context) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    @NonNull
    public static Float getDisplayWidth(@NonNull Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels / displayMetrics.density;
    }

    public static int calcNumColuns(@NonNull Context context, @NonNull Float widthView) {
        Float cols = AndroidUtils.getDisplayWidth(context) / widthView;
        return cols.intValue();

    }

    public static int calcHeight(@NonNull Context context, int margin, @NonNull Pair<Integer, Integer> proportion) {
        Float height = (getDisplayWidth(context) - margin) / (proportion.first / proportion.second);
        return height.intValue();

    }

    public static ViewGroup.LayoutParams calcHeight(@NonNull Context context, int nubColumns, int margin, ViewGroup.LayoutParams params, Pair<Integer, Integer> proportion) {
        params.height = (int) ((getDisplayWidth(context) / nubColumns) - margin) / (proportion.first / proportion.second);
        return params;

    }
}
