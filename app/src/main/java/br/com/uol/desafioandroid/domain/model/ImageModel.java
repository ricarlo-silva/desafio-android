package br.com.uol.desafioandroid.domain.model;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImageModel implements Serializable {

    @SerializedName("normal")
    private String normal;

    @SerializedName("hidpi")
    private String hidpi;

    @SerializedName("teaser")
    private String teaser;

    public String getNormal() {
        return normal;
    }

    public String getHidpi() {
        return hidpi;
    }

    public String getTeaser() {
        return teaser;
    }

    @Override
    public String toString() {
        return
                "ImageModel{" +
                        "normal = '" + normal + '\'' +
                        ",hidpi = '" + hidpi + '\'' +
                        ",teaser = '" + teaser + '\'' +
                        "}";
    }
}