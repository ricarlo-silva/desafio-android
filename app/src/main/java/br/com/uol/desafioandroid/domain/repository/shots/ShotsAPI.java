package br.com.uol.desafioandroid.domain.repository.shots;

import java.util.List;

import br.com.uol.desafioandroid.domain.model.ShotsModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ricarlosilva on 28/11/2017.
 */

public interface ShotsAPI {

    @GET("shots")
    Call<List<ShotsModel>> getShots(
            @Query("page") int page,
            @Query("sort") String sort,
            @Query("list") String list,
            @Query("access_token") String token);
}
