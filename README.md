# Criar um aplicativo de consulta na api do [Dribbble](https://dribbble.com) #

** Desafio deve ser feito somente em Java **

Criar um aplicativo para consultar a [Dribbble API](http://developer.dribbble.com/v1/) e criar listagem de shots.


### O que foi utilizado? ###

* Comunicação com API - [Retrofit](http://square.github.io/retrofit/)
* Load e cache de imagens - [Glide](https://github.com/bumptech/glide)
* Android [Material Design](https://material.io/guidelines/#introduction-principles)

### Exemplos ###

![alt text](captures/device-img-2.png "img 1")
![alt text](captures/device-img-1.png "img 2")
![alt text](captures/device-img-3.png "img 3")
![alt text](captures/device-img-4.png "img 4")

